/*
-----------------------------------------------------------------------------
Filename:    TutorialApplication.cpp
-----------------------------------------------------------------------------

This source file is part of the
   ___                 __    __ _ _    _
  /___\__ _ _ __ ___  / / /\ \ (_) | _(_)
 //  // _` | '__/ _ \ \ \/  \/ / | |/ / |
/ \_// (_| | | |  __/  \  /\  /| |   <| |
\___/ \__, |_|  \___|   \/  \/ |_|_|\_\_|
      |___/
Tutorial Framework (for Ogre 1.9)
http://www.ogre3d.org/wiki/
-----------------------------------------------------------------------------
*/

#include "TutorialApplication.h"
#include "CelestialBodies.h"
//---------------------------------------------------------------------------
TutorialApplication::TutorialApplication(void)
{
}
//---------------------------------------------------------------------------
TutorialApplication::~TutorialApplication(void)
{
}

bool TutorialApplication::frameStarted(const FrameEvent& evt)
{
    mSun->selfRotation(evt);
    mEarth->rotation(evt);
    mEarth->selfRotation(evt);
    mMercury->rotation(evt);
    mMercury->selfRotation(evt);
    mVenus->rotation(evt);
    mVenus->selfRotation(evt);
    mMars->rotation(evt);
    mMars->selfRotation(evt);
    mMoon->moonRotation(evt, mEarth->getNode()->getPosition().x,mEarth->getNode()->getPosition().z);
    return true;
}

//---------------------------------------------------------------------------
void TutorialApplication::createScene(void)
{
    // Create your scene here :)
    ColourValue color;
    CelestialBodies* sun = new CelestialBodies(20, 0, 5, 0,  "sun", mSceneMgr, "SunMesh", ColourValue(1.0, 1.0, 0.0), "sunMaterial");
    mSun = sun;
    CelestialBodies* earth = new CelestialBodies(10, 90, 35, 6, "earth", mSceneMgr, "earthMesh", ColourValue(0.0, 0.5, 0.0), "earthMaterial");
    earth->setPosition();
    mEarth = earth;
    CelestialBodies* mercury = new CelestialBodies(3, 50, 15, 24, "mercury", mSceneMgr, "mercuryMesh", ColourValue(0.9, 0.6, 0.0), "mercuryMaterial");
    mercury->setPosition();
    mMercury = mercury;
    CelestialBodies* venus = new CelestialBodies(5, 70, 25, 12, "venus", mSceneMgr, "venusMesh", ColourValue(0.1, 1.0, 0.1), "venusMaterial");
    venus->setPosition();
    mVenus = venus;
    CelestialBodies* mars = new CelestialBodies(8, 110, 45, 3, "mars", mSceneMgr, "marsMesh", ColourValue(1.0, 0.0, 0.0), "marsMaterial");
    mars->setPosition();
    mMars = mars;
    CelestialBodies* moon = new CelestialBodies(2, 105, 40, 10, "moon", mSceneMgr, "moonMesh", ColourValue(0.5, 0.5, 0.5), "moonMaterial");
    moon->setPosition();
    mMoon = moon;

    Light* pointLight = mSceneMgr->createLight();
    pointLight->setType(Light::LightTypes::LT_POINT);
    pointLight->setDiffuseColour(ColourValue(1.0f, 1.0f, 0.0f));
    pointLight->setSpecularColour(ColourValue(3.0f, 3.0f, 3.0f));
    pointLight->setAttenuation(325, 0.0f, 0.014, 0.0007);
    pointLight->setCastShadows(false);
    mSceneMgr->setAmbientLight(ColourValue(0.5f, 0.5f, 0.5f));

}
//---------------------------------------------------------------------------

#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
#define WIN32_LEAN_AND_MEAN
#include "windows.h"
#endif

#ifdef __cplusplus
extern "C" {
#endif

#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
    INT WINAPI WinMain(HINSTANCE hInst, HINSTANCE, LPSTR strCmdLine, INT)
#else
    int main(int argc, char *argv[])
#endif
    {
        // Create application object
        TutorialApplication app;

        try {
            app.go();
        } catch(Ogre::Exception& e)  {
#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
            MessageBox(NULL, e.getFullDescription().c_str(), "An exception has occurred!", MB_OK | MB_ICONERROR | MB_TASKMODAL);
#else
            std::cerr << "An exception has occurred: " <<
                e.getFullDescription().c_str() << std::endl;
#endif
        }

        return 0;
    }

#ifdef __cplusplus
}
#endif

//---------------------------------------------------------------------------
