#pragma once
#include<iostream>
#include<string>
#include "TutorialApplication.h"

using namespace std;

class CelestialBodies
{
public:
	CelestialBodies(float size, float distance, float selfRotation, float sunRotation, string name, SceneManager* manager, const std::string& strName, ColourValue color, string materialName);
	void createSpehere(const std::string& strName, const float r, const int nRings, const int nSegments, SceneManager* manager);

	void rotation(const FrameEvent & evt);
	void selfRotation(const FrameEvent & evt);
	void moonRotation(const FrameEvent& evt, float earthPosX, float earthPosZ);
	void setPosition();
	
	float getSize();
	float getDistance();
	float getSelftRotation();
	float getSunRotation();
	SceneNode* getNode();


private:
	float mSize; 
	float mDistanceSun;
	float mSelfRotation;
	float mSunRotation;
	SceneNode* mNode;
};

