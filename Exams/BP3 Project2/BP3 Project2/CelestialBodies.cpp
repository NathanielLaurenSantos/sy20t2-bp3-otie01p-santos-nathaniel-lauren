#include "CelestialBodies.h"
#include<iostream>
#include<string>
#include "TutorialApplication.h"

using namespace std;

CelestialBodies::CelestialBodies(const float size, float distance, float selfRotation, float sunRotation, string name, SceneManager* manager, const std::string& strName, ColourValue color, string materialName)
{

    mSize = size;
    mDistanceSun = distance;
    mSelfRotation = selfRotation;
    mSunRotation = sunRotation;

    MaterialPtr myManualObjectMaterial = Ogre::MaterialManager::getSingleton().create(materialName, "General");
    // Modify some properties of materials
    if ( name== "sun") {
        myManualObjectMaterial->getTechnique(0)->setLightingEnabled(false);
    }
    myManualObjectMaterial->getTechnique(0)->getPass(0)->setDiffuse(color);
    myManualObjectMaterial->getTechnique(0)->getPass(0)->setSpecular(0.0, 0.0, 0.0, 0);
    
    
    createSpehere(strName, size, 16, 16, manager);
    Entity* sphereEntity = manager->createEntity(name, strName);
    SceneNode* sphereNode = manager->getRootSceneNode()->createChildSceneNode();

        sphereEntity->setMaterialName(materialName);

    sphereNode->attachObject(sphereEntity);
    mNode = sphereNode;
    
}

/*Taken From Ogre3d Wiki,  http://wiki.ogre3d.org/ManualSphereMeshes#Example */
void CelestialBodies::createSpehere(const std::string& strName, const float r, const int nRings, const int nSegments, SceneManager* manager)
{
    MeshPtr pSphere = MeshManager::getSingleton().createManual(strName, ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME);
    SubMesh* pSphereVertex = pSphere->createSubMesh();

    pSphere->sharedVertexData = new VertexData();
    VertexData* vertexData = pSphere->sharedVertexData;

    // define the vertex format
    VertexDeclaration* vertexDecl = vertexData->vertexDeclaration;
    size_t currOffset = 0;
    // positions
    vertexDecl->addElement(0, currOffset, VET_FLOAT3, VES_POSITION);
    currOffset += VertexElement::getTypeSize(VET_FLOAT3);
    // normals
    vertexDecl->addElement(0, currOffset, VET_FLOAT3, VES_NORMAL);
    currOffset += VertexElement::getTypeSize(VET_FLOAT3);
    // two dimensional texture coordinates
    vertexDecl->addElement(0, currOffset, VET_FLOAT2, VES_TEXTURE_COORDINATES, 0);
    currOffset += VertexElement::getTypeSize(VET_FLOAT2);

    // allocate the vertex buffer
    vertexData->vertexCount = (nRings + 1) * (nSegments + 1);
    HardwareVertexBufferSharedPtr vBuf = HardwareBufferManager::getSingleton().createVertexBuffer(vertexDecl->getVertexSize(0), vertexData->vertexCount, HardwareBuffer::HBU_STATIC_WRITE_ONLY, false);
    VertexBufferBinding* binding = vertexData->vertexBufferBinding;
    binding->setBinding(0, vBuf);
    float* pVertex = static_cast<float*>(vBuf->lock(HardwareBuffer::HBL_DISCARD));

    // allocate index buffer
    pSphereVertex->indexData->indexCount = 6 * nRings * (nSegments + 1);
    pSphereVertex->indexData->indexBuffer = HardwareBufferManager::getSingleton().createIndexBuffer(HardwareIndexBuffer::IT_16BIT, pSphereVertex->indexData->indexCount, HardwareBuffer::HBU_STATIC_WRITE_ONLY, false);
    HardwareIndexBufferSharedPtr iBuf = pSphereVertex->indexData->indexBuffer;
    unsigned short* pIndices = static_cast<unsigned short*>(iBuf->lock(HardwareBuffer::HBL_DISCARD));

    float fDeltaRingAngle = (Math::PI / nRings);
    float fDeltaSegAngle = (2 * Math::PI / nSegments);
    unsigned short wVerticeIndex = 0;

    // Generate the group of rings for the sphere
    for (int ring = 0; ring <= nRings; ring++) {
        float r0 = r * sinf(ring * fDeltaRingAngle);
        float y0 = r * cosf(ring * fDeltaRingAngle);

        // Generate the group of segments for the current ring
        for (int seg = 0; seg <= nSegments; seg++) {
            float x0 = r0 * sinf(seg * fDeltaSegAngle);
            float z0 = r0 * cosf(seg * fDeltaSegAngle);

            // Add one vertex to the strip which makes up the sphere
            *pVertex++ = x0;
            *pVertex++ = y0;
            *pVertex++ = z0;

            Vector3 vNormal = Vector3(x0, y0, z0).normalisedCopy();
            *pVertex++ = vNormal.x;
            *pVertex++ = vNormal.y;
            *pVertex++ = vNormal.z;

            *pVertex++ = (float)seg / (float)nSegments;
            *pVertex++ = (float)ring / (float)nRings;

            if (ring != nRings) {
                // each vertex (except the last) has six indices pointing to it
                *pIndices++ = wVerticeIndex + nSegments + 1;
                *pIndices++ = wVerticeIndex;
                *pIndices++ = wVerticeIndex + nSegments;
                *pIndices++ = wVerticeIndex + nSegments + 1;
                *pIndices++ = wVerticeIndex + 1;
                *pIndices++ = wVerticeIndex;
                wVerticeIndex++;
            }
        }; // end for seg
    } // end for ring

    // Unlock
    vBuf->unlock();
    iBuf->unlock();
    // Generate face list
    pSphereVertex->useSharedVertices = true;

    // the original code was missing this line:
    pSphere->_setBounds(AxisAlignedBox(Vector3(-r, -r, -r), Vector3(r, r, r)), false);
    pSphere->_setBoundingSphereRadius(r);
    // this line makes clear the mesh is loaded (avoids memory leaks)
    pSphere->load();
}

void CelestialBodies::rotation(const FrameEvent& evt)
{
    Degree rotationSpeed = Degree(this->getSunRotation() * evt.timeSinceLastFrame);
    float newX =(mNode->getPosition().x * Math::Cos(Radian(rotationSpeed))) + mNode->getPosition().z * Math::Sin(Radian(rotationSpeed));
    float newZ = (mNode->getPosition().x * -Math::Sin(Radian(rotationSpeed))) + mNode->getPosition().z * Math::Cos(Radian(rotationSpeed));
    mNode->setPosition(newX, 0, newZ);
}

void CelestialBodies::selfRotation(const FrameEvent& evt)
{
    Radian rotationSpeed = Radian(Degree(this->getSelftRotation() * evt.timeSinceLastFrame));
    mNode->rotate(Vector3(0, 1, 0), rotationSpeed);
}

void CelestialBodies::moonRotation(const FrameEvent& evt, float earthPosX, float earthPosZ)
{
    Degree rotationSpeed = Degree(this->getSunRotation() * evt.timeSinceLastFrame);
    
    float newX = ((mNode->getPosition().x - earthPosX)* Math::Cos(Radian(rotationSpeed))) + (mNode->getPosition() - earthPosZ).z* Math::Sin(Radian(rotationSpeed));
    float newZ = ((mNode->getPosition().x - earthPosX) * -Math::Sin(Radian(rotationSpeed))) + (mNode->getPosition().z - earthPosZ) * Math::Cos(Radian(rotationSpeed));
    newX = newX + earthPosX;
    newZ = newZ + earthPosZ;
    mNode->setPosition(newX, 0, newZ);
    
}

void CelestialBodies::setPosition()
{
    mNode->translate(Vector3(this->getDistance(), 0, 0));
}

float CelestialBodies::getSize()
{
	return mSize;
}

float CelestialBodies::getDistance()
{
	return mDistanceSun;
}

float CelestialBodies::getSelftRotation()
{
	return mSelfRotation;
}

float CelestialBodies::getSunRotation()
{
	return mSunRotation;
}

SceneNode* CelestialBodies::getNode()
{
    return mNode;
}
