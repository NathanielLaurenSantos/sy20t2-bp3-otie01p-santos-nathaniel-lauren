/*
-----------------------------------------------------------------------------
Filename:    TutorialApplication.cpp
-----------------------------------------------------------------------------

This source file is part of the
   ___                 __    __ _ _    _
  /___\__ _ _ __ ___  / / /\ \ (_) | _(_)
 //  // _` | '__/ _ \ \ \/  \/ / | |/ / |
/ \_// (_| | | |  __/  \  /\  /| |   <| |
\___/ \__, |_|  \___|   \/  \/ |_|_|\_\_|
      |___/
Tutorial Framework (for Ogre 1.9)
http://www.ogre3d.org/wiki/
-----------------------------------------------------------------------------
*/

#include "TutorialApplication.h"
#include "CelestialBodies.h"
//---------------------------------------------------------------------------
TutorialApplication::TutorialApplication(void)
{
}
//---------------------------------------------------------------------------
TutorialApplication::~TutorialApplication(void)
{
}

bool TutorialApplication::frameStarted(const FrameEvent& evt)
{
    mSun->selfRotation(evt);
    mEarth->rotation(evt);
    mEarth->selfRotation(evt);
    mMercury->rotation(evt);
    mMercury->selfRotation(evt);
    mVenus->rotation(evt);
    mVenus->selfRotation(evt);
    mMars->rotation(evt);
    mMars->selfRotation(evt);
    mMoon->moonRotation(evt, mEarth->getNode()->getPosition().x,mEarth->getNode()->getPosition().z);
    return true;
}

//---------------------------------------------------------------------------
void TutorialApplication::createScene(void)
{
    // Create your scene here :)

    CelestialBodies* sun = new CelestialBodies(20, 0, 5, 0, ColourValue(1.00, 0.800, 0,  0.800), "sun", mSceneMgr);
    mSun = sun;
    CelestialBodies* earth = new CelestialBodies(10, 150, 35, 6, ColourValue::Green, "earth", mSceneMgr);
    earth->setPosition();
    mEarth = earth;
    CelestialBodies* mercury = new CelestialBodies(3, 50, 15, 24, ColourValue(1.0, 0.425,0.0, 0.875), "mercury", mSceneMgr);
    mercury->setPosition();
    mMercury = mercury;
    CelestialBodies* venus = new CelestialBodies(5, 100, 25, 12, ColourValue(0,1,1), "venus", mSceneMgr);
    venus->setPosition();
    mVenus = venus;
    CelestialBodies* mars = new CelestialBodies(8, 200, 45, 3, ColourValue::Red, "mars", mSceneMgr);
    mars->setPosition();
    mMars = mars;
    CelestialBodies* moon = new CelestialBodies(2, 175, 40, 10, ColourValue::Blue, "moon", mSceneMgr);
    moon->setPosition();
    mMoon = moon;

}
//---------------------------------------------------------------------------

#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
#define WIN32_LEAN_AND_MEAN
#include "windows.h"
#endif

#ifdef __cplusplus
extern "C" {
#endif

#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
    INT WINAPI WinMain(HINSTANCE hInst, HINSTANCE, LPSTR strCmdLine, INT)
#else
    int main(int argc, char *argv[])
#endif
    {
        // Create application object
        TutorialApplication app;

        try {
            app.go();
        } catch(Ogre::Exception& e)  {
#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
            MessageBox(NULL, e.getFullDescription().c_str(), "An exception has occurred!", MB_OK | MB_ICONERROR | MB_TASKMODAL);
#else
            std::cerr << "An exception has occurred: " <<
                e.getFullDescription().c_str() << std::endl;
#endif
        }

        return 0;
    }

#ifdef __cplusplus
}
#endif

//---------------------------------------------------------------------------
