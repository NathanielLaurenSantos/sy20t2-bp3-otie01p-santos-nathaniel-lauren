#include "CelestialBodies.h"
#include<iostream>
#include<string>
#include "TutorialApplication.h"

using namespace std;

CelestialBodies::CelestialBodies(float size, float distance, float selfRotation, float sunRotation, ColourValue color, string name, SceneManager* manager)
{

    mSize = size;
    mDistanceSun = distance;
    mSelfRotation = selfRotation;
    mSunRotation = sunRotation;

    ManualObject* planet = manager->createManualObject(name);
	planet->begin("BaseWhiteNoLighting", RenderOperation::OT_TRIANGLE_LIST);

    /*0*/planet->position(-size, -size, size);
    planet->colour(color);
    /*1*/planet->position(size, -size, size);
    planet->colour(color);
    /*2*/planet->position(size, size, size);
    planet->colour(color);
    /*3*/planet->position(-size, size, size);
    planet->colour(color);
    /*4*/planet->position(-size, -size, -size);
    planet->colour(color);
    /*5*/planet->position(size, -size, -size);
    planet->colour(color);
    /*6*/planet->position(size, size, -size);
    planet->colour(color);
    /*7*/planet->position(-size, size, -size);
    planet->colour(color);

    planet->index(0);
    planet->index(1);
    planet->index(2);

    planet->index(0);
    planet->index(2);
    planet->index(3);

    planet->index(3);
    planet->index(2);
    planet->index(6);

    planet->index(3);
    planet->index(6);
    planet->index(7);

    planet->index(1);
    planet->index(5);
    planet->index(6);

    planet->index(1);
    planet->index(6);
    planet->index(2);

    planet->index(4);
    planet->index(0);
    planet->index(3);

    planet->index(4);
    planet->index(3);
    planet->index(7);

    planet->index(4);
    planet->index(5);
    planet->index(1);

    planet->index(4);
    planet->index(1);
    planet->index(0);

    planet->index(5);
    planet->index(4);
    planet->index(7);

    planet->index(5);
    planet->index(7);
    planet->index(6);

    planet->end();
    mNode = manager->getRootSceneNode()->createChildSceneNode();
    mNode->attachObject(planet);

}

void CelestialBodies::rotation(const FrameEvent& evt)
{
    Degree rotationSpeed = Degree(this->getSunRotation() * evt.timeSinceLastFrame);
    float newX =(mNode->getPosition().x * Math::Cos(Radian(rotationSpeed))) + mNode->getPosition().z * Math::Sin(Radian(rotationSpeed));
    float newZ = (mNode->getPosition().x * -Math::Sin(Radian(rotationSpeed))) + mNode->getPosition().z * Math::Cos(Radian(rotationSpeed));
    mNode->setPosition(newX, 0, newZ);
}

void CelestialBodies::selfRotation(const FrameEvent& evt)
{
    Radian rotationSpeed = Radian(Degree(this->getSelftRotation() * evt.timeSinceLastFrame));
    mNode->rotate(Vector3(0, 1, 0), rotationSpeed);
}

void CelestialBodies::moonRotation(const FrameEvent& evt, float earthPosX, float earthPosZ)
{
    Degree rotationSpeed = Degree(this->getSunRotation() * evt.timeSinceLastFrame);
    
    float newX = ((mNode->getPosition().x - earthPosX)* Math::Cos(Radian(rotationSpeed))) + (mNode->getPosition() - earthPosZ).z* Math::Sin(Radian(rotationSpeed));
    float newZ = ((mNode->getPosition().x - earthPosX) * -Math::Sin(Radian(rotationSpeed))) + (mNode->getPosition().z - earthPosZ) * Math::Cos(Radian(rotationSpeed));
    newX = newX + earthPosX;
    newZ = newZ + earthPosZ;
    mNode->setPosition(newX, 0, newZ);
    
}

void CelestialBodies::setPosition()
{
    mNode->translate(Vector3(this->getDistance(), 0, 0));
}

float CelestialBodies::getSize()
{
	return mSize;
}

float CelestialBodies::getDistance()
{
	return mDistanceSun;
}

float CelestialBodies::getSelftRotation()
{
	return mSelfRotation;
}

float CelestialBodies::getSunRotation()
{
	return mSunRotation;
}

SceneNode* CelestialBodies::getNode()
{
    return mNode;
}
