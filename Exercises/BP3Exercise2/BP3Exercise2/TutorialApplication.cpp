/*
-----------------------------------------------------------------------------
Filename:    TutorialApplication.cpp
-----------------------------------------------------------------------------

This source file is part of the
   ___                 __    __ _ _    _
  /___\__ _ _ __ ___  / / /\ \ (_) | _(_)
 //  // _` | '__/ _ \ \ \/  \/ / | |/ / |
/ \_// (_| | | |  __/  \  /\  /| |   <| |
\___/ \__, |_|  \___|   \/  \/ |_|_|\_\_|
      |___/
Tutorial Framework (for Ogre 1.9)
http://www.ogre3d.org/wiki/
-----------------------------------------------------------------------------
*/

#include "TutorialApplication.h"
//---------------------------------------------------------------------------
TutorialApplication::TutorialApplication(void)
{
}
//---------------------------------------------------------------------------
TutorialApplication::~TutorialApplication(void)
{
}

//---------------------------------------------------------------------------
void TutorialApplication::createScene(void)
{
    // Create your scene here :)
    ManualObject* manual = createCube();
    cubeMvmnt= mSceneMgr->getRootSceneNode()->createChildSceneNode();
    cubeMvmnt->attachObject(manual);
}
ManualObject* TutorialApplication::createCube()
{
    ManualObject* cube = mSceneMgr->createManualObject("");
    cube->begin("BaseWhiteNoLighting", RenderOperation::OT_TRIANGLE_LIST);

    /*0*/cube->position(-10.0, -10.0, 10.0);
    cube->colour(ColourValue::Blue);
    /*1*/cube->position(10.0, -10.0, 10.0);
    cube->colour(ColourValue::Red);
    /*2*/cube->position(10.0, 10.0, 10.0);
    cube->colour(ColourValue::Green);
    /*3*/cube->position(-10.0, 10.0, 10.0);
    cube->colour(ColourValue::Blue);
    /*4*/cube->position(-10.0, -10.0, -10.0);
    cube->colour(ColourValue::Red);
    /*5*/cube->position(10.0, -10.0, -10.0);
    cube->colour(ColourValue::Green);
    /*6*/cube->position(10.0, 10.0, -10.0);
    cube->colour(ColourValue::Blue);
    /*7*/cube->position(-10.0, 10.0, -10.0);
    cube->colour(ColourValue::Red);

    cube->index(0);
    cube->index(1);
    cube->index(2);

    cube->index(0);
    cube->index(2);
    cube->index(3);

    cube->index(3);
    cube->index(2);
    cube->index(6);

    cube->index(3);
    cube->index(6);
    cube->index(7);

    cube->index(1);
    cube->index(5);
    cube->index(6);

    cube->index(1);
    cube->index(6);
    cube->index(2);

    cube->index(4);
    cube->index(0);
    cube->index(3);

    cube->index(4);
    cube->index(3);
    cube->index(7);

    cube->index(4);
    cube->index(5);
    cube->index(1);

    cube->index(4);
    cube->index(1);
    cube->index(0);

    cube->index(5);
    cube->index(4);
    cube->index(7);

    cube->index(5);
    cube->index(7);
    cube->index(6);

    cube->end();
    return cube;
}
bool TutorialApplication::keyPressed(const OIS::KeyEvent& arg)
{
    BaseApplication::keyPressed(arg);
    return true;
}
bool TutorialApplication::keyReleased(const OIS::KeyEvent& arg)
{
    BaseApplication::keyReleased(arg);
    if (arg.key == OIS::KeyCode::KC_I) {
        mvmntSpeed = 10.0;
    }
    if (arg.key == OIS::KeyCode::KC_J) {
        mvmntSpeed = 10.0;
    }
    if (arg.key == OIS::KeyCode::KC_K) {
        mvmntSpeed = 10.0;
    }
    if (arg.key == OIS::KeyCode::KC_L) {
        mvmntSpeed = 10.0;
    }
    return true;
}
bool TutorialApplication::frameStarted(const FrameEvent& evt)
{

    float xMove=0;
    float yMove=0;

    if (mKeyboard->isKeyDown(OIS::KeyCode::KC_I)) {
        mvmntSpeed++;
        yMove += mvmntSpeed * evt.timeSinceLastFrame;
    }
    if (mKeyboard->isKeyDown(OIS::KeyCode::KC_J)) {
        mvmntSpeed++;
        xMove -= mvmntSpeed * evt.timeSinceLastFrame;
    }
    if (mKeyboard->isKeyDown(OIS::KeyCode::KC_K)) {
        mvmntSpeed++;
        yMove -= mvmntSpeed * evt.timeSinceLastFrame;
    }
    if (mKeyboard->isKeyDown(OIS::KeyCode::KC_L)) {
        mvmntSpeed++;
        xMove += mvmntSpeed * evt.timeSinceLastFrame;
    }
    if (xMove != 0 || yMove != 0) {
        cubeMvmnt->translate(xMove, yMove, 0);
        
    }

    return true;
}
//---------------------------------------------------------------------------

#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
#define WIN32_LEAN_AND_MEAN
#include "windows.h"
#endif

#ifdef __cplusplus
extern "C" {
#endif

#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
    INT WINAPI WinMain(HINSTANCE hInst, HINSTANCE, LPSTR strCmdLine, INT)
#else
    int main(int argc, char *argv[])
#endif
    {
        // Create application object
        TutorialApplication app;

        try {
            app.go();
        } catch(Ogre::Exception& e)  {
#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
            MessageBox(NULL, e.getFullDescription().c_str(), "An exception has occurred!", MB_OK | MB_ICONERROR | MB_TASKMODAL);
#else
            std::cerr << "An exception has occurred: " <<
                e.getFullDescription().c_str() << std::endl;
#endif
        }

        return 0;
    }

#ifdef __cplusplus
}
#endif

//---------------------------------------------------------------------------
