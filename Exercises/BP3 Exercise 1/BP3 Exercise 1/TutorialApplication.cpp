/*
-----------------------------------------------------------------------------
Filename:    TutorialApplication.cpp
-----------------------------------------------------------------------------

This source file is part of the
   ___                 __    __ _ _    _
  /___\__ _ _ __ ___  / / /\ \ (_) | _(_)
 //  // _` | '__/ _ \ \ \/  \/ / | |/ / |
/ \_// (_| | | |  __/  \  /\  /| |   <| |
\___/ \__, |_|  \___|   \/  \/ |_|_|\_\_|
      |___/
Tutorial Framework (for Ogre 1.9)
http://www.ogre3d.org/wiki/
-----------------------------------------------------------------------------
*/

#include "TutorialApplication.h"
//---------------------------------------------------------------------------
TutorialApplication::TutorialApplication(void)
{
}
//---------------------------------------------------------------------------
TutorialApplication::~TutorialApplication(void)
{
}

//---------------------------------------------------------------------------
void TutorialApplication::createScene(void)
{
    // Create your scene here :)
    ManualObject* manual = createCube();
    mSceneMgr->getRootSceneNode()->createChildSceneNode()->attachObject(manual);
}
ManualObject* TutorialApplication::createCube()
{
    ManualObject* cube = mSceneMgr->createManualObject("");
    cube->begin("BaseWhiteNoLighting", RenderOperation::OT_TRIANGLE_LIST);

    /*0*/cube->position(-10.0, -10.0, 10.0);
    /*1*/cube->position(10.0, -10.0, 10.0);
    /*2*/cube->position(10.0, 10.0, 10.0);
    /*3*/cube->position(-10.0, 10.0, 10.0);
    /*4*/cube->position(-10.0, -10.0, -10.0);
    /*5*/cube->position(10.0, -10.0, -10.0);
    /*6*/cube->position(10.0, 10.0, -10.0);
    /*7*/cube->position(-10.0, 10.0, -10.0);

    cube->index(0);
    cube->index(1);
    cube->index(2);

    cube->index(0);
    cube->index(2);
    cube->index(3);

    cube->index(3);
    cube->index(2);
    cube->index(6);

    cube->index(3);
    cube->index(6);
    cube->index(7);

    cube->index(1);
    cube->index(5);
    cube->index(6);

    cube->index(1);
    cube->index(6);
    cube->index(2);

    cube->index(4);
    cube->index(0);
    cube->index(3);

    cube->index(4);
    cube->index(3);
    cube->index(7);

    cube->index(4);
    cube->index(5);
    cube->index(1);

    cube->index(4);
    cube->index(1);
    cube->index(0);

    cube->index(5);
    cube->index(4);
    cube->index(7);

    cube->index(5);
    cube->index(7);
    cube->index(6);

    cube->end();
    return cube;
}
//---------------------------------------------------------------------------

#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
#define WIN32_LEAN_AND_MEAN
#include "windows.h"
#endif

#ifdef __cplusplus
extern "C" {
#endif

#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
    INT WINAPI WinMain(HINSTANCE hInst, HINSTANCE, LPSTR strCmdLine, INT)
#else
    int main(int argc, char *argv[])
#endif
    {
        // Create application object
        TutorialApplication app;

        try {
            app.go();
        } catch(Ogre::Exception& e)  {
#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
            MessageBox(NULL, e.getFullDescription().c_str(), "An exception has occurred!", MB_OK | MB_ICONERROR | MB_TASKMODAL);
#else
            std::cerr << "An exception has occurred: " <<
                e.getFullDescription().c_str() << std::endl;
#endif
        }

        return 0;
    }

#ifdef __cplusplus
}
#endif

//---------------------------------------------------------------------------
