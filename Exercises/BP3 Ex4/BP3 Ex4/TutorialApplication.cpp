/*
-----------------------------------------------------------------------------
Filename:    TutorialApplication.cpp
-----------------------------------------------------------------------------

This source file is part of the
   ___                 __    __ _ _    _
  /___\__ _ _ __ ___  / / /\ \ (_) | _(_)
 //  // _` | '__/ _ \ \ \/  \/ / | |/ / |
/ \_// (_| | | |  __/  \  /\  /| |   <| |
\___/ \__, |_|  \___|   \/  \/ |_|_|\_\_|
      |___/
Tutorial Framework (for Ogre 1.9)
http://www.ogre3d.org/wiki/
-----------------------------------------------------------------------------
*/

#include "TutorialApplication.h"
#include <string>
//---------------------------------------------------------------------------
TutorialApplication::TutorialApplication(void)
{
}
//---------------------------------------------------------------------------
TutorialApplication::~TutorialApplication(void)
{
}

//---------------------------------------------------------------------------
void TutorialApplication::createScene(void)
{
    // Create your scene here :)
    ManualObject* manual = createCube();
    /*cubeMvmnt = mSceneMgr->getRootSceneNode()->createChildSceneNode();
    cubeMvmnt->attachObject(manual);*/
    
    cubeNode= mSceneMgr->getRootSceneNode()->createChildSceneNode();
    cubeNode->attachObject(manual);

    cubeNode->translate(Vector3(20,0,0));

    Light* pointLight = mSceneMgr->createLight();
    pointLight->setType(Light::LightTypes::LT_POINT);
    pointLight->setDiffuseColour(ColourValue(1.0f, 1.0f, 0.0f));
    pointLight->setSpecularColour(ColourValue(1.0f, 1.0f, 1.0f));
    pointLight->setAttenuation(325, 0.0f, 0.014, 0.0007);
    pointLight->setCastShadows(false);


    mSceneMgr->setAmbientLight(ColourValue(0.5f, 0.5f, 0.5f));
}
ManualObject* TutorialApplication::createCube()
{
    ManualObject* cube = mSceneMgr->createManualObject("");
    cube->begin("BaseWhite", RenderOperation::OT_TRIANGLE_LIST);
    //Front
    /*0*/cube->position(-10.0, -10.0, 10.0);
    cube->normal(Vector3(0, 0, 1));
    /*1*/cube->position(10.0, -10.0, 10.0);
    cube->normal(Vector3(0, 0, 1));
    /*2*/cube->position(10.0, 10.0, 10.0);
    cube->normal(Vector3(0, 0, 1));
    /*3*/cube->position(-10.0, 10.0, 10.0);
    cube->normal(Vector3(0, 0, 1));

    //Top
    /*4*/cube->position(10.0, 10.0, 10.0);
    cube->normal(Vector3(0, 1, 0));
    /*5*/cube->position(-10.0, 10.0, 10.0);
    cube->normal(Vector3(0, 1, 0));
    /*6*/cube->position(10.0, 10.0, -10.0);
    cube->normal(Vector3(0, 1, 0));
    /*7*/cube->position(-10.0, 10.0, -10.0);
    cube->normal(Vector3(0, 1, 0));

    //Bottom
    /*8*/cube->position(-10.0, -10.0, 10.0);
    cube->normal(Vector3(0, -1, 0));
    /*9*/cube->position(10.0, -10.0, 10.0);
    cube->normal(Vector3(0, -1, 0));
    /*10*/cube->position(-10.0, -10.0, -10.0);
    cube->normal(Vector3(0, -1, 0));
    /*11*/cube->position(10.0, -10.0, -10.0);
    cube->normal(Vector3(0, -1, 0));

    //Back
    /*12*/cube->position(-10.0, -10.0, -10.0);
    cube->normal(Vector3(0, 0, -1));
    /*13*/cube->position(10.0, -10.0, -10.0);
    cube->normal(Vector3(0, 0, -1));
    /*14*/cube->position(10.0, 10.0, -10.0);
    cube->normal(Vector3(0, 0, -1));
    /*15*/cube->position(-10.0, 10.0, -10.0);
    cube->normal(Vector3(0, 0, -1));

    //Left
    /*16*/cube->position(-10.0, 10.0, -10.0);
    cube->normal(Vector3(-1, 0, 0));
    /*17*/cube->position(-10.0, -10.0, -10.0);
    cube->normal(Vector3(-1, 0, 0));
    /*18*/cube->position(-10.0, 10.0, 10.0);
    cube->normal(Vector3(-1, 0, 0));
    /*19*/cube->position(-10.0, -10.0, 10.0);
    cube->normal(Vector3(-1, 0, 0));

    //Right
    /*16*/cube->position(10.0, -10.0, 10.0);
    cube->normal(Vector3(1, 0, 0));
    /*17*/cube->position(10.0, 10.0, 10.0);
    cube->normal(Vector3(1, 0, 0));
    /*18*/cube->position(10.0, -10.0, -10.0);
    cube->normal(Vector3(1, 0, 0));
    /*19*/cube->position(10.0, 10.0, -10.0);
    cube->normal(Vector3(1, 0, 0));

    //Front
    cube->index(0);
    cube->index(1);
    cube->index(2);
    cube->index(0);
    cube->index(2);
    cube->index(3);

    //Right
    cube->index(20);
    cube->index(22);
    cube->index(23);
    cube->index(20);
    cube->index(23);
    cube->index(21);

    //Left
    cube->index(17);
    cube->index(19);
    cube->index(18);
    cube->index(17);
    cube->index(18);
    cube->index(16);

    //Top
    cube->index(5);
    cube->index(4);
    cube->index(6);
    cube->index(5);
    cube->index(6);
    cube->index(7);

    //Bottom
    cube->index(10);
    cube->index(11);
    cube->index(9);
    cube->index(10);
    cube->index(9);
    cube->index(8);

    //Back
    cube->index(13);
    cube->index(12);
    cube->index(15);
    cube->index(13);
    cube->index(15);
    cube->index(14);

    cube->end();
    return cube;
}
 
bool TutorialApplication::frameStarted(const FrameEvent& evt)
{
    Radian rotation = Radian(Degree(30 * evt.timeSinceLastFrame));

    if (mKeyboard->isKeyDown(OIS::KeyCode::KC_NUMPAD8)) {
        cubeNode->rotate(Vector3(1, 0, 0), rotation);
    }
    if (mKeyboard->isKeyDown(OIS::KeyCode::KC_NUMPAD2)) {
        cubeNode->rotate(Vector3(-1, 0, 0), rotation);
    }
    if (mKeyboard->isKeyDown(OIS::KeyCode::KC_NUMPAD4)) {
        cubeNode->rotate(Vector3(0, -1, 0), rotation);
    }
    if (mKeyboard->isKeyDown(OIS::KeyCode::KC_NUMPAD6)) {
        cubeNode->rotate(Vector3(0, 1, 0), rotation);
    }
    

    return true;
}
//---------------------------------------------------------------------------

#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
#define WIN32_LEAN_AND_MEAN
#include "windows.h"
#endif

#ifdef __cplusplus
extern "C" {
#endif

#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
    INT WINAPI WinMain(HINSTANCE hInst, HINSTANCE, LPSTR strCmdLine, INT)
#else
    int main(int argc, char *argv[])
#endif
    {
        // Create application object
        TutorialApplication app;

        try {
            app.go();
        } catch(Ogre::Exception& e)  {
#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
            MessageBox(NULL, e.getFullDescription().c_str(), "An exception has occurred!", MB_OK | MB_ICONERROR | MB_TASKMODAL);
#else
            std::cerr << "An exception has occurred: " <<
                e.getFullDescription().c_str() << std::endl;
#endif
        }

        return 0;
    }

#ifdef __cplusplus
}
#endif

//---------------------------------------------------------------------------
